# prime number checker app

# prerequisites
- Python image
- Flask module

# steps in CI/CD pipeline
- initiate runner with python latest image
- define stages
    - build_job
    - deploy_job
- define build stage
    - install Flask using pip
    - store module using artifact
- define deploy stage 
    - run app.py
    - return null to terminate the process
